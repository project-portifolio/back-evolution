package util;

import java.math.BigInteger;
import java.security.MessageDigest;

import org.springframework.stereotype.Component;

/**
 * Descrição: Classe responsavel por encriptar a senha do usuario usando o padrão MD5
 * @author "Fábio Campêllo"
 * @since 14/07/2018
 * @version 0.0.1 
 */
@Component
public class CriptPassword {

	/**
	 * Método que gera a senha encriptada do usuario
	 * @param String senha
	 * @return String password
	 * @throws Exception se erro em encriptar
	 */
	public static String encriptografarSenha(String senha) {

		String password = "";

		MessageDigest md;
		
		try {
			md = MessageDigest.getInstance("MD5");
			BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
			password = hash.toString(16);
		} catch (Exception err) {
			System.out.println("Erro tipo: " + err.getMessage());
		}

		return password;
	}

}
