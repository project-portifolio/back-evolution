package util;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.project.evolution.model.HToken;
import com.project.evolution.model.Usuario;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Descrição: Classe que gera o token do usuario
 * @author "Fábio Campêllo"
 * @since 25/07/2018
 * @version 0.0.1 
 */
@Component
public class TokenUtil {

	/**
	 * Método responsável por gerar o JsonWebToken
	 * @param user o usuario
	 * @return String token
	 */
	public static HToken gerarToken(Usuario user) {

		/**
		 * INSTANCIANDO OBJETO JSONWEBTOKEN
		 */
		JwtBuilder builder = Jwts.builder();

		/**
		 * BUILDING UM TOKEN E SALVANDO NUMA STRING
		 */
		String userToken = builder.setId(user.getId().toString()).setSubject(user.getEmail())
				.signWith(SignatureAlgorithm.HS512, "evolution")
				.setExpiration(new Date(System.currentTimeMillis() + 9 * 60 * 60 * 1000)).compact();

		/**
		 * INSTANCIANDO UMA CLASSE PARA GUARDAR TEMPORARIAMENTE O TOKEN
		 */
		HToken token = new HToken();

		/**
		 * SETANDO NUMA CLASSE EM TEMPO DE EXECUCAO
		 */
		token.setUserToken(userToken);
		return token;
	}

}
