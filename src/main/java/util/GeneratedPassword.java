package util;

import java.util.Random;

import org.springframework.stereotype.Component;

/**
 * Descrição: Classe responsável por gerar código/senha de seis digitos
 * 
 * @author Fábio Campêllo
 * @since 17/04/2019
 * @version 0.0.1
 */
@Component
public class GeneratedPassword {

	/**
	 * Responsável por gerar números randomicos
	 */
	static Random rand;

	/**
	 * Tamanho do código/senha
	 */
	static final int TAM_SENHA = 1000000;

	static String password = "";

	public static String gerandoSenha() {
		rand = new Random();

		password = Integer.toString(rand.nextInt(TAM_SENHA + 1));

		return password;
	}

}
