package com.project.evolution.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.evolution.dto.EmailDTO;
import com.project.evolution.service.EmailService;

/**
 * Descrição: End-point da api responsável pelo envio de e-mails
 * 
 * @author "Fábio Campêllo"
 * @since 25/01/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/sendEmail")
public class EmailController {

	@Autowired
	private EmailService emailService;

	/**
	 * Descrição: Método que envia e-mail para um ou mais associados, parametrizados 
	 * através de atributos do EmailDTO
	 * 
	 * @param EmailDTO emailDTO
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "listEmails", method = RequestMethod.POST)
	public String sendMail(@RequestBody EmailDTO emailDTO) {
		int i = emailDTO.getListEmails().length;
		try {
			for (int count = 0; count < i; count++) {
				String titulo = emailDTO.getTitulo();
				String msg = emailDTO.getMsg();
				String email = emailDTO.getListEmails()[count];
				emailService.sendMail(titulo, msg, email);
			}
			return new String("{\"success\":1}");
		} catch (Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}

	/**
	 * Descrição: Método que envia e-mail para um ou mais associados, parametrizados 
	 * através de atributos do EmailDTO, com anexo de arquivo (EM PROGRESSO)
	 * 
	 * @param EmailDTO emailDTO
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "emailAnexo", method = RequestMethod.POST)
	public String sendMailAnexo(@RequestBody EmailDTO emailDTO) {
		int i = emailDTO.getListEmails().length;
		try {
			for (int count = 0; count < i; count++) {
				String titulo = emailDTO.getTitulo();
				String msg = emailDTO.getMsg();
				String email = emailDTO.getListEmails()[count];
				String base64 = emailDTO.getUrlFile();
				String nameFile = emailDTO.getNameFile();
				emailService.sendMailAnexo(titulo, msg, email, base64, nameFile);
			}
			return new String("{\"success\":1}");
		} catch (Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}

}