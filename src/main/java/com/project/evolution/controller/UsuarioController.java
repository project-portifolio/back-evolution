package com.project.evolution.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.evolution.dto.UserDTO;
import com.project.evolution.model.HToken;
import com.project.evolution.model.Usuario;
import com.project.evolution.service.EmailService;
import com.project.evolution.service.UsuarioService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import util.CriptPassword;
import util.GeneratedPassword;
import util.TokenUtil;

/**
 * Descrição: End-point da api responsável por requisicoes do usuario.
 * 
 * @author "Fábio Campêllo"
 * @since 06/07/2018
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private EmailService emailService;

	@SuppressWarnings("finally")
	@ApiOperation(
			value = "API de cadastro de Usuario",
			notes = "API de cadastro de Usuario.",
			response = Usuario.class,
			responseContainer = "Boolean",
			produces = "application/json")
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Retorna um boolean com valor true."),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
		    @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@RequestMapping(value = "cadastrarUsuario", method = RequestMethod.POST)
	public Boolean cadastrar(@RequestBody Usuario user) {
		String senhaEncriptada = CriptPassword.encriptografarSenha(user.getSenha());
		user.setSenha(senhaEncriptada);
		Boolean userLogin = false;
		try {
			userLogin = usuarioService.userByEmail(user.getEmail());
		} catch (Exception error) {
			error.printStackTrace();
		} finally {
			if (userLogin == false) {
				usuarioService.cadastrarUsuario(user);
				return userLogin;
			} else {
				return userLogin;
			}

		}
	}

	/**
	 * Descrição: Método que trata do fluxo de login na aplicacao
	 * 
	 * @param Usuario user
	 * @return Usuario user
	 */
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ResponseEntity<Usuario> login(@RequestBody Usuario user) {

		Boolean usuarioJaCadadastrado = usuarioService.userByEmail(user.getEmail());

		if (usuarioJaCadadastrado == true) {

			Usuario userRetorno = null;

			String senhaEncriptada = CriptPassword.encriptografarSenha(user.getSenha());
			user.setSenha(senhaEncriptada);
			userRetorno = usuarioService.usuarioAutenticado(user.getEmail(), user.getSenha());
			try {

				HToken token = TokenUtil.gerarToken(userRetorno);
				userRetorno.setToken(token);
				userRetorno.setSenha(null);
				return new ResponseEntity<Usuario>(userRetorno, HttpStatus.OK);
			} catch (Exception error) {
				error.printStackTrace();
				return new ResponseEntity<Usuario>(user, HttpStatus.BAD_REQUEST);
			}

		} else {
			System.out.println("Usuário não existe na base!");
			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * Descrição: Verifica se existe usuário na base de dados
	 * 
	 * @param Usuario user
	 * @return boolean true or false
	 */
	@RequestMapping(value = "verifica", method = RequestMethod.POST)
	public Boolean verificaUsuario(@RequestBody Usuario user) {
		Boolean isUser = false;
		String codigo = null;
		String msg = "O código de recuperação de senha é: ";
		try {
			isUser = usuarioService.userByEmail(user.getEmail());
			if(isUser) {
				codigo = GeneratedPassword.gerandoSenha();
				usuarioService.setRecoveryPassword(user.getEmail(), codigo);
				emailService.sendMail("Recuperação de senha", msg + codigo, user.getEmail());
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
		return isUser;
	}

	/**
	 * Descrição: Buscar todos os usuários
	 * 
	 * @return List<Usuario> usuarios
	 */
	@RequestMapping(value = "getAllUsers", method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> getAllUsers() {
		ResponseEntity<List<Usuario>> list = null;
		try {
			list = new ResponseEntity<List<Usuario>>(usuarioService.getAllUsers(), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			list = new ResponseEntity<List<Usuario>>(HttpStatus.BAD_REQUEST);
		}
		return list;
	}

	/**
	 * Descrição: Contando usuários cadastrados
	 * 
	 * @return int count
	 */
	@RequestMapping(value = "countAllUsers", method = RequestMethod.GET)
	public int countUsers() {
		int count = 0;
		try {
			count = usuarioService.countAllUsers();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * Descrição: Salvando lista de usuários importados de arquivo excel

	 * @param List<Usuario> usuarios
	 * @return List<Usuario> usuarios
	 */
	@RequestMapping(value = "saveListUsers", method = RequestMethod.PUT)
	public ResponseEntity<List<Usuario>> saveListUsers(@RequestBody List<Usuario> usuarios) {
		ResponseEntity<List<Usuario>> list = null;
		try {
			list = new ResponseEntity<List<Usuario>>(usuarioService.saveListUsers(usuarios), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}

	/**
	 * Descrição: Atualizando status do usuario
	 * 
	 * @param boolean status
	 * @param Long id
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "updateStatus/{status}/{id}", method = RequestMethod.GET)
	public String updateStatus(@PathVariable("status") boolean status, @PathVariable("id") Long id) {
		try {
			String senha = usuarioService.getSenha(id);
			usuarioService.updateStatus(status, id, senha);
			return new String("{\"success\":1}");
		} catch (Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}

	/**
	 * Descrição: Removendo usuario
	 * 
	 * @param Long id
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "removeUser/{id}", method = RequestMethod.DELETE)
	public String removeUser(@PathVariable("id") Long id) {
		try {
			usuarioService.removeUser(id);
			return new String("{\"success\":1}");
		} catch (Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}

	/**
	 * Descrição: Retornando usuario pelo id
	 * 
	 * @param Long id
	 * @return Usuario usuario
	 */
	@RequestMapping(value = "findUserById/{id}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> findUserById(@PathVariable("id") Long id) {
		ResponseEntity<Usuario> user = null;
		try {
			user = new ResponseEntity<Usuario>(usuarioService.findUserById(id), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

	/**
	 * Descrição: Editando o usuario
	 * 
	 * @param Usuario usuario
	 * @return Usuario usuario
	 */
	@RequestMapping(value = "updateUser", method = RequestMethod.POST)
	public ResponseEntity<Usuario> updateUser(@RequestBody Usuario usuario) {
		ResponseEntity<Usuario> user = null;
		try {
			usuario.setSenha(usuarioService.getSenha(usuario.getId()));		
			user = new ResponseEntity<Usuario>(usuarioService.updateUser(usuario), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}
	
	/**
	 * Descrição: Vinculando permissao ao usuario
	 * 
	 * @param UserDTO userDTO
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "linkPermissaoUser", method = RequestMethod.PUT)
	public String linkPermissaoUser(@RequestBody UserDTO userDTO) {
		System.out.println("UserDTO: " + userDTO);
		try {
			usuarioService.linkPermissaoUser(userDTO);
			return new String("{\"success\":1}");
		} catch(Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}
	
	/**
	 * Descrição: Confirma codigo de recuperacao de senha
	 * 
	 * @param Usuario user
	 * @return boolean true or false
	 */
	@RequestMapping(value = "confirmRecovery", method = RequestMethod.POST)
	public Boolean confirmRecovery(@RequestBody Usuario user) {
		Boolean isTrue = false;
		try {
			isTrue = usuarioService.getCodigo(user.getCodigo());
		} catch(Exception error) {
			error.printStackTrace();
		}
		return isTrue;
	}
	
	/**
	 * Descrição: Atualiza a senha
	 * 
	 * @param Usuario usuario
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "setNewPassword", method = RequestMethod.POST)
	public String setNewPassword(@RequestBody Usuario user) {
		String senhaEncriptada = CriptPassword.encriptografarSenha(user.getSenha());
		try {
			usuarioService.setNewPassword(user.getEmail(), senhaEncriptada);
			return new String("{\"success\":1}");
		} catch(Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}
	
}
