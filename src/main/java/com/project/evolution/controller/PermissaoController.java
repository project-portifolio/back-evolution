package com.project.evolution.controller;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.evolution.model.Permissao;
import com.project.evolution.service.PermissaoService;

/**
 * Descrição: End-point da api responsável pelas permissoes dos usuario
 * 
 * @author "Fábio Campêllo"
 * @since 12/01/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/permissao")
public class PermissaoController {

	@Autowired
	private PermissaoService permissaoService;

	/**
	 * Descrição: Salvando /atualizando a permissao
	 * 
	 * @param Permissao permissao
	 * @return Permissao permissao
	 */
	@RequestMapping(value = "updateSavePermissao", method = RequestMethod.POST)
	public ResponseEntity<Permissao> getUpdateSavePermissao(@RequestBody Permissao permissao) {
		Permissao permissaoRetorno = null;
		try {
			permissaoRetorno = permissaoService.updateSavePermissao(permissao);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return new ResponseEntity<Permissao>(permissaoRetorno, HttpStatus.OK);
	}

	/**
	 * Descrição: Buscando todas as permissoes
	 * 
	 * @return List<Permissao> permissoes
	 */
	@RequestMapping(value = "getAllPermissoes", method = RequestMethod.GET)
	public ResponseEntity<List<Permissao>> getAllMotoristaVales() {
		try {
			return new ResponseEntity<List<Permissao>>(permissaoService.getAllPermissoes(), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<List<Permissao>>(HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * Descrição: Retornando permissão pelo id
	 * 
	 * @param Long id
	 * @return Permissao permissao
	 */
	@RequestMapping(value = "getPermissaoById/{id}", method = RequestMethod.GET)
	public ResponseEntity<Permissao> getPermissaoById(@PathVariable("id") Long id) {
		try {
			return new ResponseEntity<Permissao>(permissaoService.getPermissaoById(id), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<Permissao>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Descrição: Removendo permissao
	 * 
	 * @param Long id
	 * @return String success 1
	 */
	@RequestMapping(value = "removePermissao/{id}", method = RequestMethod.DELETE)
	public String removePermissao(@PathVariable("id") Long id) {
		String msgErro = "";
		try {
			permissaoService.removePermissao(id);
			msgErro = "{\"success\":1}";
		} catch (ConstraintViolationException error) {
			error.printStackTrace();
			msgErro = error.toString();
		}
		return msgErro;
	}

}
