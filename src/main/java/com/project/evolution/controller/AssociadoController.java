package com.project.evolution.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.evolution.model.Associado;
import com.project.evolution.service.AssociadoService;

/**
 * Descrição: End-point da api responsável por requisicoes do associado.
 * 
 * @author "Fábio Campêllo"
 * @since 19/01/2019
 * @version 0.0.1
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/associados")
public class AssociadoController {

	@Autowired
	private AssociadoService associadoService;

	/**
	 * Descrição: Cadastrando e atualizando associado
	 * 
	 * @param Associado associado
	 * @return Associado associado
	 */
	@RequestMapping(value = "updateSaveAssociado", method = RequestMethod.POST)
	public ResponseEntity<Associado> updateSaveAssociado(@RequestBody Associado associado) {
		Associado a = null;
		try {
			a = associadoService.updateSaveAssociado(associado);
			return new ResponseEntity<Associado>(a, HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<Associado>(HttpStatus.BAD_REQUEST);
		}
		
	}

	/**
	 * Descrição: Buscando todos os associados
	 * 
	 * @return List<Associado> associados
	 */
	@RequestMapping(value = "getAll", method = RequestMethod.GET)
	public ResponseEntity<List<Associado>> getAll() {
		try {
			return new ResponseEntity<List<Associado>>(associadoService.getAll(), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<List<Associado>>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Descrição: Buscando todos os asssociados passando o status como parâmetro ordenando pela matrícula 
	 * 
	 * @param status
	 * @return List<Associado> associados
	 */
	@RequestMapping(value = "findAllStatusOrderByMatricula/{status}", method = RequestMethod.GET)
	public ResponseEntity<List<Associado>> findAllStatusOrderByMatricula(@PathVariable("status") Boolean status) {
		try {
			return new ResponseEntity<List<Associado>>(associadoService.findAllStatusOrderByMatricula(status), HttpStatus.OK);
		} catch(Exception error) {
			error.printStackTrace();
			return new ResponseEntity<List<Associado>>(associadoService.findAllStatusOrderByMatricula(status), HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Descrição: Buscando associado pelo id
	 * 
	 * @param Long id
	 * @return Associado associado
	 */
	@RequestMapping(value = "findAssociadoById/{id}", method = RequestMethod.GET)
	public Associado findAssociadoById(@PathVariable("id") Long id) {
		Associado associado = null;
		try {
			associado = associadoService.findAssociadoById(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return associado;
	}

	/**
	 * Descrição: Contando todos os associados
	 * 
	 * @return int count
	 */
	@RequestMapping(value = "countAllAssociados", method = RequestMethod.GET)
	public int countAllAssociados() {
		int count = 0;
		try {
			count = associadoService.countAllAssociados();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * Descrição: Contando todos os assiociados ativos
	 * 
	 * @return int count
	 */
	@RequestMapping(value = "countAllAssociadosAtivos", method = RequestMethod.GET)
	public int countAllAssociadosAtivos() {
		int count = 0;
		try {
			count = associadoService.countAllAssociadosAtivos();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * Descrição: Contando todos os associados inativos
	 * 
	 * @return int count
	 */
	@RequestMapping(value = "countAllAssociadosInativos", method = RequestMethod.GET)
	public int countAllAssociadosInativos() {
		int count = 0;
		try {
			count = associadoService.countAllAssociadosInativos();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * Descrição: Salvando lista de associados importados de arquivo excel
	 * 
	 * @param List<Associado> associados
	 * @return List<Associado> associados
	 */
	@RequestMapping(value = "saveListAssociados", method = RequestMethod.PUT)
	public ResponseEntity<List<Associado>> saveListAssociados(@RequestBody List<Associado> associados) {
		ResponseEntity<List<Associado>> list = null;
		try {
			list = new ResponseEntity<List<Associado>>(associadoService.saveListAssociados(associados), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			list = new ResponseEntity<List<Associado>>(HttpStatus.BAD_REQUEST);
		}
		return list;
	}

	/**
	 * Descrição: Buscando associado pelo nome
	 * 
	 * @param String nome
	 * @return List<Associado> associados
	 */
	@RequestMapping(value = "getAssociadoByLikeNome/{nome}", method = RequestMethod.GET)
	public ResponseEntity<List<Associado>> getAssociadoByLikeNome(@PathVariable("nome") String nome) {
		try {
			return new ResponseEntity<List<Associado>>(associadoService.getAssociadoByLikeNome(nome), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<List<Associado>>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Descrição: Buscando associado pela lotação
	 * 
	 * @param String lotacao
	 * @return List<Associado> associados
	 */
	@RequestMapping(value = "getAssociadoByLotacao/{lotacao}", method = RequestMethod.GET)
	public ResponseEntity<List<Associado>> getAssociadoByLotacao(@PathVariable("lotacao") String lotacao) {
		try {
			return new ResponseEntity<List<Associado>>(associadoService.getAssociadoByLotacao(lotacao), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<List<Associado>>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Descrição: Excluindo associado
	 * 
	 * @param Long id
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "removeAssociado/{id}", method = RequestMethod.DELETE)
	public String removeAssociado(@PathVariable("id") Long id) {
		try {
			associadoService.removeAssociado(id);
			return new String("{\"success\":1}");
		} catch (Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}

	/**
	 * Descrição: Atualizando status do associado
	 * 
	 * @param boolean status
	 * @param Long id
	 * @return String success: 1 or 0
	 */
	@RequestMapping(value = "updateStatus/{status}/{id}", method = RequestMethod.GET)
	public String updateStatus(@PathVariable("status") boolean status, @PathVariable("id") Long id) {
		try {
			associadoService.updateStatus(status, id);
			return new String("{\"success\":1}");
		} catch (Exception error) {
			error.printStackTrace();
			return new String("{\"success\":0}");
		}
	}
	
	/**
	 * Descrição: Buscando associado filtrado pela matrícula
	 * 
	 * @param Long matricula
	 * @return List<Associado> associados
	 */
	@RequestMapping(value = "filterByMatricula/{matricula}", method = RequestMethod.GET)
	public List<Associado> filterByMatricula(@PathVariable("matricula") Long matricula) {
		List<Associado> list = null;
		try {
			list = associadoService.filterByMatricula(matricula);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}
	
	/**
	 * retornando a última matrícula
	 * 
	 * @return Long matricula
	 */
	@RequestMapping(value = "findLastByMatricula", method = RequestMethod.GET)
	public Long findLastByMatricula() {
		Long matricula = 0L;
		try {
			matricula = associadoService.findLastByMatricula();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return matricula;
	}
	
	/**
	 * buscando aniversariantes por parâmetro passado 
	 * 
	 * @param dataInicio
	 * @param dataFim
	 * @return
	 */
	@RequestMapping(value = "getListAniversariantes/{dataInicio}/{dataFim}", method = RequestMethod.GET)
	public List<Associado> getListAniversariantes(@PathVariable("dataInicio") String dataInicio, @PathVariable("dataFim") String dataFim) {
		List<Associado> aniversariantes = null;
		try {
			aniversariantes = associadoService.getListAniversariantes(dataInicio, dataFim);
		} catch(Exception error) {
			error.printStackTrace();
		}
		return aniversariantes;
	}

}
