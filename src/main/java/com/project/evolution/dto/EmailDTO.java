package com.project.evolution.dto;

import java.util.Arrays;

/**
 * Descrição: Classe que representa a passagem dos parâmetros do envio de email
 * ao associado, representa o JSON que vem do front. JSON EmailDTO {titulo:'',
 * msg: '', listEmails: ''}
 * 
 * @author "Fábio Campêllo"
 * @since 27/01/2019
 * @version 0.0.1
 */
public class EmailDTO {

	/**
	 * ATRIBUTOS DE CLASSE
	 */
	private String titulo;

	private String msg;

	private String[] listEmails;

	private String nameFile;

	private String urlFile;

	/**
	 * MÉTODOS ACESSORES
	 */
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String[] getListEmails() {
		return listEmails;
	}

	public void setListEmails(String[] listEmails) {
		this.listEmails = listEmails;
	}

	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	public String getUrlFile() {
		return urlFile;
	}

	public void setUrlFile(String urlFile) {
		this.urlFile = urlFile;
	}

	/**
	 * MÉTODO TOSTRING
	 */
	@Override
	public String toString() {
		return "EmailDTO [titulo=" + titulo + ", msg=" + msg + ", listEmails=" + Arrays.toString(listEmails)
				+ ", nameFile=" + nameFile + ", urlFile=" + urlFile + "]";
	}

}
