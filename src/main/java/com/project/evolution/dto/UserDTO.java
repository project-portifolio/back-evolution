package com.project.evolution.dto;

import java.util.Arrays;

/**
 * Descrição: Classe que representa a passagem de dados do vinculo de usuário
 * com permissao, ou seja, representa o JSON que vem do front. JSON UserDTO
 * {id:'',[{id}]}
 * 
 * @author "Fábio Campêllo"
 * @since 13/01/2019
 * @version 0.0.1
 */
public class UserDTO {

	/**
	 * ATRIBUTOS DE CLASSE
	 */
	private Long id;

	private Long[] idPermissao;

	/**
	 * MÉTODOS ACESSORES
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long[] getIdPermissao() {
		return idPermissao;
	}

	public void setIdPermissao(Long[] idPermissao) {
		this.idPermissao = idPermissao;
	}

	/**
	 * MÉTODO TOSTRING
	 */
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", idPermissao=" + Arrays.toString(idPermissao) + "]";
	}

}
