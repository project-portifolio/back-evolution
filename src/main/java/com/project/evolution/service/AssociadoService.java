package com.project.evolution.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.evolution.model.Associado;
import com.project.evolution.repository.AssociadoRepository;

/**
 * Descrição: Classe de serviço do associado, implementa regra de negócio
 * conforme a demanda e dependendo da necessidade implementa na camada de
 * persistência o JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 19/01/2019
 * @version 0.0.1
 */
@Service
public class AssociadoService {

	@Autowired
	private AssociadoRepository associadoRepository;

	/**
	 * cadastrando e atualizando associado
	 * 
	 * @param associado
	 * @return a
	 * @throws Exception
	 */
	public Associado updateSaveAssociado(Associado associado) {
		Associado a = null;
		try {
			a = associadoRepository.save(associado);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return a;
	}

	/**
	 * buscando todos os associados
	 * 
	 * @return list
	 */
	public List<Associado> getAll() {
		List<Associado> list = null;
		try {
			list = associadoRepository.findAllOrderByMatricula();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}
	
	/**
	 * buscando todos os asssociados passando o status como parâmetro ordenando pela matrícula 
	 * 
	 * @param status
	 * @return list
	 */
	public List<Associado> findAllStatusOrderByMatricula(Boolean status) {
		List<Associado> list = null;
		try {
			list = associadoRepository.findAllStatusOrderByMatricula(status);
		} catch(Exception error) {
			error.printStackTrace();
		}
		return list;
	}

	/**
	 * buscando associado pelo id
	 * 
	 * @param id
	 * @return associado
	 */
	public Associado findAssociadoById(Long id) {
		Associado associado = null;
		try {
			associado = associadoRepository.findAssociadoById(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return associado;
	}

	/**
	 * contando todos os associados
	 * 
	 * @return count
	 */
	public int countAllAssociados() {
		int count = 0;
		try {
			count = associadoRepository.countAllAssociados();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * contando todos os assiociados ativos
	 * 
	 * @return count
	 */
	public int countAllAssociadosAtivos() {
		int count = 0;
		try {
			count = associadoRepository.countAllAssociadosAtivos();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * contando todos os associados inativos
	 * 
	 * @return count
	 */
	public int countAllAssociadosInativos() {
		int count = 0;
		try {
			count = associadoRepository.countAllAssociadosInativos();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * salvando lista de associados importados de arquivo excel
	 * 
	 * @param associados
	 * @return list
	 */
	public List<Associado> saveListAssociados(List<Associado> associados) {
		List<Associado> list = null;
		try {
			for (int i = 0; i <= associados.size(); i++) {
				int count = 0;
				String cpf = associados.get(i).getCpf();
				count = associadoRepository.getAssociadoByCpf(cpf);
				if (count < 1) {
					associadoRepository.save(associados.get(i));
				}
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}

	/**
	 * buscando associado pelo nome
	 * 
	 * @param nome
	 * @return list
	 */
	public List<Associado> getAssociadoByLikeNome(String nome) {
		List<Associado> list = new ArrayList<>();
		try {
			list = associadoRepository.getAssociadoByLikeNome(nome);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}

	/**
	 * buscando associado pela lotação
	 * 
	 * @param lotacao
	 * @return list
	 */
	public List<Associado> getAssociadoByLotacao(String lotacao) {
		List<Associado> list = new ArrayList<>();
		try {
			list = associadoRepository.getAssociadoByLotacao(lotacao);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}

	/**
	 * excluindo associado
	 * 
	 * @param id
	 */
	public void removeAssociado(Long id) {
		try {
			associadoRepository.delete(id);
		} catch (Exception error) {
			error.printStackTrace();
		}

	}

	/**
	 * atualizando status do associado
	 * 
	 * @param status
	 * @param id
	 */
	public void updateStatus(boolean status, Long id) {
		try {
			if (status == true) {
				associadoRepository.updateInativarStatus(id);
			} else {
				associadoRepository.updateAtivarStatus(id);
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	
	/**
	 * buscando todo associado filtrado pela matrícula 
	 * 
	 * #param Long matricula
	 * @return list
	 */
	public List<Associado> filterByMatricula(Long matricula) {
		List<Associado> list = null;
		try {
			list = associadoRepository.filterByMatricula(matricula);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	} 
	
	/**
	 * retornando a última matrícula
	 * 
	 * @return Long matricula
	 */
	public Long findLastByMatricula() {

		Long matricula = 0L;
		try {
			matricula = associadoRepository.findOneLastByMatricula();
		} catch(Exception error) {
			error.printStackTrace();
		} 
		return matricula;
	}
	
	/**
	 * buscando aniversariantes por parâmetro passado 
	 * 
	 * @param dataInicio
	 * @param dataFim
	 * @return
	 */
	public List<Associado> getListAniversariantes(String dataInicio, String dataFim) {
		List<Associado> aniversariantes = null;
		try {
			aniversariantes = associadoRepository.getListAniversariantes(dataInicio, dataFim);
		} catch(Exception error) {
			error.printStackTrace();
		}
		return aniversariantes;
	}

}
