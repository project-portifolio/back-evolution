package com.project.evolution.service;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.evolution.model.Permissao;
import com.project.evolution.repository.PermissaoRepository;

/**
 * Descrição: Classe de serviço da permissão, implementa regra de negócio
 * conforme a demanda e dependendo da necessidade implementa na camada de
 * persistência o JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 12/01/2019
 * @version 0.0.1
 */
@Service
public class PermissaoService {

	@Autowired
	private PermissaoRepository permissaoRepository;

	public Permissao updateSavePermissao(Permissao permissao) {
		Permissao p = null;
		try {
			p = permissaoRepository.save(permissao);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return p;
	}

	public List<Permissao> getAllPermissoes() {
		List<Permissao> permissoes = null;
		try {
			permissoes = permissaoRepository.findAll();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return permissoes;
	}

	public Permissao getPermissaoById(Long id) {
		Permissao permissao = null;
		try {
			permissao = permissaoRepository.findOne(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return permissao;
	}

	public void removePermissao(Long id) throws ConstraintViolationException {
		permissaoRepository.delete(id);
	}

}
