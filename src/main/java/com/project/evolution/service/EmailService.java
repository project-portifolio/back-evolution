package com.project.evolution.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender mailSender;

	public void sendMail(String titulo, String msg, String email) {
		MimeMessage mail = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(mail);
			helper.setTo(InternetAddress.parse(email));
			helper.setSubject(titulo);
			helper.setText(msg);
			mailSender.send(mail);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	public void sendMailAnexo(String titulo, String msg, String email, String base64, String nameFile) throws IOException {

		MimeMessage message = mailSender.createMimeMessage();
		OutputStream os = null;
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			if(base64 != null) {
				String[] strings = base64.split(",");
				String extension = getTypeFile(strings[0]);
				
				if( extension.equals("other") ) {
					try {
						throw new Exception("Erro ao persistir objeto.");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			
				// convert string base64 to binary data
				byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
				String fileName = "CoolImage." + extension;

				File file = new File(fileName);

				try {
					os = new FileOutputStream(file);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				os.write(data);
				helper.addAttachment(nameFile, file);
			}
			
			helper.setTo(InternetAddress.parse(email));
			helper.setSubject(titulo);
			helper.setText(msg);

			mailSender.send(message);
		} catch (MessagingException e) {

			e.printStackTrace();
		}

	}
	
	private String getTypeFile(String StringFile) {
		String extensionFile = "";
		
		switch (StringFile) {
		
			case "data:application/jpeg;base64":
				extensionFile = "jpeg";
				break;
				
			case "data:application/jpg;base64":
				extensionFile = "jpg";
				break;
				
			case "data:application/png;base64":
				extensionFile = "png";
				break;
				
			case "data:application/pdf;base64":
				extensionFile = "pdf";
				break;
				
			case "data:text/plain;base64":
				extensionFile = "txt";
				break;
				
			case "data:application/msword;base64":
				extensionFile = "doc";
				break;
				
			case "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64":
				extensionFile = "docx";
				break;
				
			case "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64":
				extensionFile = "xlsx";
				break;
			
			case "data:image/gif;base64":
				extensionFile = "gif";
				break;
				
			default:
				extensionFile = "other";
				break;
		}
		
		return extensionFile;
	}

}
