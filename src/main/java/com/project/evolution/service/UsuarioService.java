package com.project.evolution.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.evolution.dto.UserDTO;
import com.project.evolution.model.Usuario;
import com.project.evolution.repository.UsuarioRepository;

import util.CriptPassword;

/**
 * Descrição: Classe de serviço do usuário, implementa regra de negócio conforme
 * a demanda e dependendo da necessidade implementa na camada de persistência o
 * JPA ou JDBC.
 * 
 * @author "Fábio Campêllo"
 * @since 06/07/2018
 * @version 0.0.1
 */
@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	/**
	 * cadastrar usuário
	 * 
	 * @param user
	 */
	public void cadastrarUsuario(Usuario user) {
		try {
			usuarioRepository.save(user);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	/**
	 * verifica se o usuário existe na base
	 * 
	 * @param email
	 * @return
	 */
	public Boolean userByEmail(String email) {
		int count = 0;
		boolean isFalse = false;
		try {
			count = usuarioRepository.isUsuarioByEmail(email);
			if (count > 0) {
				isFalse = true;
			} else {
				isFalse = false;
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
		return isFalse;
	}

	/**
	 * busca usuário by id
	 * 
	 * @param email
	 * @param senha
	 * @return
	 */
	public Usuario usuarioAutenticado(String email, String senha) {
		Usuario user = null;
		try {
			user = usuarioRepository.getUsuarioAutenticado(email, senha);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

	/**
	 * buscar todos os usuários
	 * 
	 * @return
	 */
	public List<Usuario> getAllUsers() {

		List<Object[]> objetos = usuarioRepository.getAllUsers();
		List<Usuario> lista = new ArrayList<Usuario>();
		Usuario user = null;
		for (Object[] o : objetos) {
			user = new Usuario();
			try {
				user.setId(new Long(o[0].toString()));
				user.setNome(o[1].toString());
				user.setEmail(o[2].toString());
				user.setFone(o[3].toString());
				user.setNivel(new Integer(o[4].toString()));
				user.setStatus(new Boolean(o[5].toString()));
			} catch (Exception error) {
				error.printStackTrace();
			}
			lista.add(user);
		}
		return lista;
	}

	/**
	 * contando usuários cadastrados
	 * 
	 * @return
	 */
	public int countAllUsers() {
		int count = 0;
		try {
			count = usuarioRepository.countAllUsers();
		} catch (Exception error) {
			error.printStackTrace();
		}
		return count;
	}

	/**
	 * salvando lista de usuários
	 * 
	 * @param usuarios
	 * @return
	 */
	public List<Usuario> saveListUsers(List<Usuario> usuarios) {
		List<Usuario> list = null;
		try {
			list = usuarioRepository.save(usuarios);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return list;
	}

	/**
	 * atualizando status do usuário
	 * 
	 * @param status
	 * @param id
	 * @param senha
	 */
	public void updateStatus(boolean status, Long id, String senha) {

		try {
			if (status == true && (senha != null || senha == null)) {
				usuarioRepository.updateInativarStatus(id);
			} else if (status == false && senha != null) {
				usuarioRepository.updateAtivarStatus(id);
			} else if (status == false && senha == null) {
				senha = CriptPassword.encriptografarSenha("1");
				usuarioRepository.setPassword(id, senha);
			} else {
				usuarioRepository.updateAtivarStatus(id);
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	/**
	 * excluindo usuário
	 */
	public void removeUser(Long id) {
		try {
			usuarioRepository.delete(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	/**
	 * retornando usuário pelo id
	 */
	public Usuario findUserById(Long id) {
		Usuario user = null;
		try {
			user = usuarioRepository.findUserById(id);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

	/**
	 * editando o usuário
	 * 
	 * @param usuario
	 * @return
	 */
	public Usuario updateUser(Usuario usuario) {
		Usuario user = null;
		try {
			user = usuarioRepository.save(usuario);
		} catch (Exception error) {
			error.printStackTrace();
		}
		return user;
	}

	/**
	 * obtendo a senha do usuário
	 * 
	 * @param id
	 * @return
	 */
	public String getSenha(Long id) {
		String senha = usuarioRepository.getSenha(id);
		return senha;
	}

	/**
	 * vinculando permissão ao usuário
	 * 
	 * @param userDTO
	 */
	public void linkPermissaoUser(UserDTO userDTO) {
		System.out.println("UserDTO: " + userDTO);
		System.out.println(userDTO.getIdPermissao());
		int count = 0;
		for (int i = 0; i < userDTO.getIdPermissao().length; i++) {
			try {
				count = usuarioRepository.islinkPermissao(userDTO.getId(), userDTO.getIdPermissao()[i]);
				if (count < 1) {
					usuarioRepository.linkPermissaoUser(userDTO.getId(), userDTO.getIdPermissao()[i]);
				}
			} catch (ConstraintViolationException error) {
				error.printStackTrace();
			}
		}
	}
	
	/**
	 * Descrição: Atualizando o código de recuperação de senha
	 * 
	 * @param id
	 * @param codigo
	 */
	public void setRecoveryPassword(String email, String codigo) {
		try {
			usuarioRepository.setRecoveryPassword(email, codigo);
		} catch(Exception error) {
			error.printStackTrace();
		}
	}
	
	/**
	 * Descrição: Confirmando o código de recuperação de senha
	 * 
	 * @param codigo
	 * @return
	 */
	public Boolean getCodigo(String codigo) {
		int count = 0;
		boolean isFalse = false;
		try {
			count = usuarioRepository.getCodigo(codigo);
			if (count > 0) {
				isFalse = true;
			} else {
				isFalse = false;
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
		return isFalse;
	} 
	
	/**
	 * Descrição: Atualiza a senha
	 * 
	 * @param email
	 * @param senha
	 */
	public void setNewPassword(String email, String senha) {
		try {
			usuarioRepository.setNewPassword(email, senha);
		} catch(Exception error) {
			error.printStackTrace();
		}
	}

}
