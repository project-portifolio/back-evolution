package com.project.evolution.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.project.evolution.model.Associado;

public interface AssociadoRepository extends JpaRepository<Associado, Long> {

	@Query(value = "SELECT * FROM tb_associado WHERE id = :id", nativeQuery = true)
	public Associado findAssociadoById(@Param("id") Long id);

	@Query(value = "SELECT COUNT(id) FROM tb_associado", nativeQuery = true)
	public int countAllAssociados();

	@Query(value = "SELECT COUNT(id) FROM tb_associado WHERE status = true", nativeQuery = true)
	public int countAllAssociadosAtivos();

	@Query(value = "SELECT COUNT(id) FROM tb_associado WHERE status = false OR status IS NULL", nativeQuery = true)
	public int countAllAssociadosInativos();

	@Query(value = "SELECT COUNT(cpf) FROM tb_associado a WHERE a.cpf = :cpf", nativeQuery = true)
	public int getAssociadoByCpf(@Param("cpf") String cpf);

	@Query(value = "SELECT * FROM tb_associado a WHERE a.name like CONCAT('%', :nome,'%')", nativeQuery = true)
	public List<Associado> getAssociadoByLikeNome(@Param("nome") String nome);

	@Query(value = "SELECT * FROM tb_associado a WHERE a.lotacao like CONCAT('%', :lotacao,'%')", nativeQuery = true)
	public List<Associado> getAssociadoByLotacao(@Param("lotacao") String lotacao);

	// atualizando status associado para TRUE
	@Modifying
	@Transactional
	@Query(value = "UPDATE tb_associado a SET a.status = true WHERE a.id =:id", nativeQuery = true)
	public void updateAtivarStatus(@Param("id") Long id);

	// atualizando status associado para FALSE
	@Modifying
	@Transactional
	@Query(value = "UPDATE tb_associado a SET a.status = false WHERE a.id = :id", nativeQuery = true)
	public void updateInativarStatus(@Param("id") Long id);
	
	// buscando todos os associados ordenando pela matrícula
	@Query(value = "SELECT * FROM tb_associado ORDER BY matricula ", nativeQuery = true)
	public List<Associado> findAllOrderByMatricula();
	
	// buscando todos os asssociados passando o status como parâmetro ordenando pela matrícula
	@Query(value="SELECT * FROM tb_associado AS a WHERE a.status = :status", nativeQuery = true)
	public List<Associado> findAllStatusOrderByMatricula(@Param("status") Boolean status);
	
	// filtrando associado pela matrícula
	@Query(value = "SELECT * FROM tb_associado WHERE matricula = :matricula", nativeQuery = true)
	public List<Associado> filterByMatricula(@Param("matricula") Long matricula);
	
	// retornando a última matrícula
	@Query(value = "SELECT a.matricula FROM tb_associado AS a ORDER BY a.matricula DESC LIMIT 1;", nativeQuery = true)
	public Long findOneLastByMatricula();
	
	// buscando aniversariantes
	@Query(value="SELECT * FROM tb_associado AS a "
			+ "WHERE DATE(a.data_nascimento) BETWEEN :dataInicio AND :dataFim "
			+ "ORDER BY DATE(a.data_nascimento);", nativeQuery = true)
	public List<Associado> getListAniversariantes(@Param("dataInicio") String dataInicio, @Param("dataFim") String dataFim);
	
}
