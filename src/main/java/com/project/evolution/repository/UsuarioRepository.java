package com.project.evolution.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.project.evolution.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	// verifica se o usuário existe na base
	@Query(value = "SELECT COUNT(email) FROM tb_usuario WHERE email = :email", nativeQuery = true)
	public int isUsuarioByEmail(@Param("email") String email);

	// autentica usuário se status true
	@Query(value = "SELECT * FROM tb_usuario WHERE email = :email AND senha = :senha", nativeQuery = true)
	public Usuario getUsuarioAutenticado(@Param("email") String email, @Param("senha") String senha);

	// busca todos os usuários
	@Query(value = "select u.id, u.name, u.email, u.fone, u.nivel, u.status from tb_usuario as u;", nativeQuery = true)
	public List<Object[]> getAllUsers();

	// contando usuários cadastrados
	@Query(value = "SELECT COUNT(id) FROM tb_usuario", nativeQuery = true)
	public int countAllUsers();

	// selecionando usuário pelo id
	@Query(value = "SELECT * FROM tb_usuario u WHERE u.id = :id", nativeQuery = true)
	public Usuario findUserById(@Param("id") Long id);

	// buscando a senha do usuário pelo id
	@Query(value = "SELECT u.senha FROM tb_usuario u WHERE u.id = :id", nativeQuery = true)
	public String getSenha(@Param("id") Long id);

	// atualizando status usuário para TRUE
	@Modifying
	@Transactional
	@Query(value = "UPDATE tb_usuario u SET u.status = true WHERE u.id =:id", nativeQuery = true)
	public void updateAtivarStatus(@Param("id") Long id);

	// atualizando status usuário para FALSE
	@Modifying
	@Transactional
	@Query(value = "UPDATE tb_usuario u SET u.status = false WHERE u.id = :id", nativeQuery = true)
	public void updateInativarStatus(@Param("id") Long id);

	// atualizando senha se usuário ativo
	@Modifying
	@Transactional
	@Query(value = "UPDATE tb_usuario u SET u.status = true, u.senha = :senha WHERE u.id = :id", nativeQuery = true)
	public void setPassword(@Param("id") Long id, @Param("senha") String senha);

	// verificando se existe usuário vinculado à permissão
	@Query(value = "SELECT COUNT(usuario_id) FROM usuario_permissao WHERE usuario_id = :id AND permissao_id = :idPermissao", nativeQuery = true)
	public int islinkPermissao(@Param("id") Long id, @Param("idPermissao") Long idPermissao);

	// atualizando senha se usuário ativo
	@Modifying
	@Transactional
	@Query(value = "INSERT INTO usuario_permissao (usuario_id, permissao_id) VALUES (:id, :idPermissao)", nativeQuery = true)
	public void linkPermissaoUser(@Param("id") Long id, @Param("idPermissao") Long idPermissao);

	/**
	 * Descrição: Atualizando o código de recuperação de senha
	 * 
	 * @param email
	 * @param codigo
	 */
	@Modifying
	@Transactional
	@Query(value = "UPDATE tb_usuario u SET u.codigo = :codigo WHERE u.email = :email", nativeQuery = true)
	public void setRecoveryPassword(@Param("email") String email, @Param("codigo") String codigo);

	/**
	 * Descrição: Confirmando o código de recuperação de senha
	 * @param 
	 * @return
	 */
	@Query(value = "SELECT COUNT(u.codigo) FROM tb_usuario u WHERE u.codigo = :codigo", nativeQuery = true)
	public int getCodigo(@Param("codigo") String codigo);
	
	/**
	 * Descrição: Atualiza a senha
	 * 
	 * @param email
	 * @param senha
	 */
	@Modifying
	@Transactional
	@Query(value = "UPDATE tb_usuario u SET u.senha = :senha WHERE u.email = :email", nativeQuery = true)
	public void setNewPassword(@Param("email") String email, @Param("senha") String senha);

}
