package com.project.evolution.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.evolution.model.Permissao;

public interface PermissaoRepository extends JpaRepository<Permissao, Long> {

}
