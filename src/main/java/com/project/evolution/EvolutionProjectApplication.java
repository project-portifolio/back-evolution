package com.project.evolution;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Descrição: Classe principal do projeto Spring
 * 
 * @author "Fábio Campêllo"
 * @since 06/07/2018
 * @version 0.0.1
 */
@SpringBootApplication
public class EvolutionProjectApplication extends SpringBootServletInitializer {
	
	@PostConstruct
    void started() {
      TimeZone.setDefault(TimeZone.getTimeZone("TimeZone"));
    }

	public static void main(String[] args) {
		try {
			SpringApplication.run(EvolutionProjectApplication.class, args);
		} catch (ExceptionInInitializerError error) {
			error.printStackTrace();
		} catch (Exception error) {
			error.printStackTrace();
		}

	}
}
