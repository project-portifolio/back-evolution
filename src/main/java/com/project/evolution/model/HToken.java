package com.project.evolution.model;

import java.io.Serializable;

/**
 * Descrição: Classe concreta. Representa o objeto Token do usuário.
 * 
 * @author "Fábio Campêllo"
 * @since 25/07/2018
 * @version 0.0.1
 */
public class HToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * VARIAVEL QUE GUARDA TEMPORARIAMENTE O TOKEN DO USUARIO
	 */
	private String userToken;

	/**
	 * METODOS ACESSORES
	 */
	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
