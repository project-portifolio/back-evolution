package com.project.evolution.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

/**
 * Descrição: Classe concreta que herda da classe abstrata Pessoa. Representa o
 * objeto e a tabela usuário no sistema.
 * 
 * @author "Fábio Campêllo"
 * @since 06/07/2018
 * @version 0.0.1
 */
@Entity
@Table(name = "tb_usuario", uniqueConstraints = {
		@UniqueConstraint(name = "usuario_email", columnNames = { "email" }) })
public class Usuario extends Pessoa implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ATRIBUTOS DE CLASSE
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nivel")
	private int nivel;

	@Column(name = "senha")
	private String senha;

	@OneToMany
	@JoinColumn(name = "id_endereco")
	private Endereco endereco;

	/**
	 * ANOTAÇÃO QUE INDICA QUE ESTE DADO NÃO DEVE SER PERSISTIDO NO BANCO
	 */
	@Transient
	private HToken token;

	@ManyToMany
	@JoinTable(name = "usuario_permissao", joinColumns = @JoinColumn(name = "usuario_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "permissao_id", referencedColumnName = "id"), uniqueConstraints = @UniqueConstraint(columnNames = {
			"usuario_id", "permissao_id" }))
	private List<Permissao> permissao;

	@Size(min = 6, max = 6)
	private String codigo;

	/**
	 * MÉTODOS ACESSORES
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public HToken getToken() {
		return token;
	}

	public void setToken(HToken token) {
		this.token = token;
	}

	public List<Permissao> getPermissao() {
		return permissao;
	}

	public void setPermissao(List<Permissao> permissao) {
		this.permissao = permissao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * MÉTODO TOSTRING
	 */
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nivel=" + nivel + ", senha=" + senha + ", endereco=" + endereco + ", token="
				+ token + ", permissao=" + permissao + "]";
	}

	/**
	 * HASHCODE
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * EQUALS
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
