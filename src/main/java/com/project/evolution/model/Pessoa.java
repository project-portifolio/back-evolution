package com.project.evolution.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Descrição: Super classe abstrata que agrupa dados comuns à outros objetos no
 * sistema. Para isso, bastando que herdem desta classe.
 * 
 * @author "Fábio Campêllo"
 * @since 15/12/2018
 * @version 0.0.1
 */
@MappedSuperclass
public abstract class Pessoa {

	/**
	 * ATRIBUTOS DE CLASSE
	 */
	@NotNull
	@Column(name = "name")
	private String nome;

	@Column(name = "email")
	private String email;

	@Column(name = "fone")
	private String fone;

	@Column(name = "cpf")
	private String cpf;

	@Column(name = "data_nascimento")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Calendar dataNascimento;

	@Column(name = "status")
	private Boolean status;

	@OneToOne
	@JoinColumn(name = "id_endereco")
	private Endereco endereco;

	/**
	 * MÉTODOS ACESSORES
	 */
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/**
	 * MÉTODO TOSTRING
	 */
	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", email=" + email + ", fone=" + fone + ", cpf=" + cpf + ", dataNascimento="
				+ dataNascimento + ", status=" + status + ", endereco=" + endereco + "]";
	}

}
